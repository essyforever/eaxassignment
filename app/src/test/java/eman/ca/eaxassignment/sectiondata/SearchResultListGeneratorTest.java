package eman.ca.eaxassignment.sectiondata;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

import eman.ca.eaxassignment.model.Movie;
import eman.ca.eaxassignment.model.ResultList;
import eman.ca.eaxassignment.recyclerview.AbstractRecyclerSection;

import static org.hamcrest.CoreMatchers.is;

/**
 * Created by Esmond on 2018-08-30.
 */

public class SearchResultListGeneratorTest {

    @Test
    public void validate_generateSections() {
        {
            ResultList resultList = new ResultList();
            resultList.setPage(1);

            ArrayList<Movie> movies = new ArrayList<>();
            movies.add(new Movie());
            resultList.setMovieList(movies);

            ArrayList<AbstractRecyclerSection> sections = SearchResultListGenerator.generateSections(resultList, null);

            Assert.assertThat(sections.size(), is(2));
            Assert.assertTrue(sections.get(0) instanceof MovieListHeaderSection);
            Assert.assertTrue(sections.get(1) instanceof MovieListSection);
        }
        {
            ResultList resultList = new ResultList();
            resultList.setPage(2);

            ArrayList<Movie> movies = new ArrayList<>();
            movies.add(new Movie());
            resultList.setMovieList(movies);

            ArrayList<AbstractRecyclerSection> sections = SearchResultListGenerator.generateSections(resultList, null);

            Assert.assertThat(sections.size(), is(1));
            Assert.assertTrue(sections.get(0) instanceof MovieListSection);
        }

    }
}
