package eman.ca.eaxassignment.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Esmond on 2018-08-26.
 */

public abstract class AbstractRecyclerSection {
    public static final int DEFAULT_SPAN_COUNT = 1;

    private int mHeaderViewType = 0;

    protected int mSpanCount = DEFAULT_SPAN_COUNT;

    public AbstractRecyclerSection(Enum viewType) {
        this(viewType, DEFAULT_SPAN_COUNT);
    }

    public AbstractRecyclerSection(Enum viewType, int spanCount) {
        setHeaderViewType(viewType.ordinal());
        setSpanCount(spanCount);
    }

    public int getItemViewType() {
        return getHeaderViewType();
    }

    public abstract void bindSectionHeaderViewHolder(RecyclerView.ViewHolder sectionViewHolder, int sectionIndex);

    public void setHeaderViewType(int sectionHeaderViewType) {
        mHeaderViewType = sectionHeaderViewType;
    }

    public int getHeaderViewType() {
        return mHeaderViewType;
    }

    public int getSpanCount() {
        return mSpanCount;
    }

    public void setSpanCount(int spanCount) {
        mSpanCount = spanCount;
    }

    public long getItemId() {
        return getItemViewType();
    }

    public abstract RecyclerView.ViewHolder createViewHolder(ViewGroup parent);

    public abstract int getLayout();

    public View createView(ViewGroup parent) {
        return LayoutInflater.from(parent.getContext()).inflate(getLayout(), parent, false);
    }
}
