package eman.ca.eaxassignment.recyclerview;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by Esmond on 2018-08-26.
 */

public class RecyclerViewAdapter <T extends AbstractRecyclerSection>
        extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    protected ArrayList<AbstractRecyclerSection> mSections = null;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        for(int i=0; i<mSections.size(); i++) {
            AbstractRecyclerSection section = mSections.get(i);
            if(section.getItemViewType() == viewType) {
                return section.createViewHolder(parent);
            }
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        return mSections.get(position).getItemViewType();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        mSections.get(position).bindSectionHeaderViewHolder(viewHolder, position);
    }

    @Override
    public int getItemCount() {
        return mSections != null ? mSections.size() : 0;
    }

    public void setSections(ArrayList<AbstractRecyclerSection> sections) {
        mSections = sections;
        notifyDataSetChanged();
    }

    public void addSections(ArrayList<AbstractRecyclerSection> sections) {
        mSections.addAll(sections);
        notifyDataSetChanged();
    }
}