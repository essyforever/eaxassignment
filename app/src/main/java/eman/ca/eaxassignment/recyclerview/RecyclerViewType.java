package eman.ca.eaxassignment.recyclerview;

/**
 * Created by Esmond on 2018-08-26.
 */

public enum RecyclerViewType {
    MOVIE_LIST_HEADER,
    MOVIE_LIST_ITEM
}
