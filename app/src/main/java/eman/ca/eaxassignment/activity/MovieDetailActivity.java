package eman.ca.eaxassignment.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import eman.ca.eaxassignment.R;
import eman.ca.eaxassignment.fragment.MovieDetailFragment;

/**
 * Created by Esmond on 2018-08-30.
 */

public class MovieDetailActivity extends AppCompatActivity {

	private FragmentManager mFragmentManager;
	private Fragment mFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fragment_placeholder);

		mFragmentManager = getSupportFragmentManager();
		showFragment();
	}

	public void showFragment() {
		if(mFragment == null) {
			mFragment = MovieDetailFragment.newInstance(getIntent().getExtras());
		}

		FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
		fragmentTransaction.replace(R.id.fragment_placeholder, mFragment);
		fragmentTransaction.commit();
		mFragmentManager.executePendingTransactions();
	}
}
