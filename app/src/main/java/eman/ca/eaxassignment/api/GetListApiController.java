package eman.ca.eaxassignment.api;

import eman.ca.eaxassignment.model.ResultList;
import eman.ca.eaxassignment.utils.Const;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Esmond on 2018-08-29.
 */

public class GetListApiController implements Callback<ResultList> {

	public enum SearchResultState {
        FreshSearch,
        ShowResult,
        ShowFirstPageError,
    }

    public interface GetListService {
        @Headers({
                "Authorization: Bearer " + Const.READ_ACCESS_TOKEN,
                "Content-Type: " + Const.CONTENT_TYPE
        })
        @GET("list/{list_id}")
        Call<ResultList> getList(@Path("list_id") String listId, @Query("page") int page, @Query("sort_by") String sortBy);
    }

	private GetListService mGetListService;
	private Call<ResultList> mResultListCall;
	private Callback<ResultList> mCallback;
	private int mPage = 0;
	private String mListId;
	private String mSortBy;
	private boolean mHasNextPage = true;
	private boolean mIsDownloading = false;
	private boolean mLastCallHasError = false;

    public GetListApiController() {
	    Retrofit retrofit = new Retrofit.Builder()
			    .baseUrl(Const.BASE_URL)
			    .addConverterFactory(GsonConverterFactory.create())
			    .build();

	    mGetListService = retrofit.create(GetListService.class);
	}

	public void setRetrofitCallback(Callback<ResultList> callback) {
		mCallback = callback;
	}

	public int getPage() {
    	return mPage;
	}

	public boolean isDownloading() {
    	return mIsDownloading;
	}

	public boolean hasNextPage() {
    	return mHasNextPage;
	}

	public boolean isLastCallHasError() {
    	return mLastCallHasError;
	}

    /**
     * Do Search for the List, used for first search, afterwards can use
     * method downloadNextPageIfIdleAndNoError or downloadNextPageIfIdleAndIgnoreError in
     * {@link eman.ca.eaxassignment.sectiondata.SearchResultListGenerator}
     * @param listId
     * @param page
     * @param sortBy
     */
	public void doSearch(String listId, int page, String sortBy) {
		mPage = page;
		mListId = listId;
		mSortBy = sortBy;
        reset();
		makeApiCall();
	}

	private void makeApiCall() {
		mIsDownloading = true;
		mResultListCall = mGetListService.getList(mListId, mPage, mSortBy);
		mResultListCall.enqueue(this);
	}

    /**
     * If Api is not downloading and has next page and there was no error then make api call
     */
	public void downloadNextPageIfIdleAndNoError() {
    	if(!mIsDownloading && mHasNextPage && !mLastCallHasError) {
		    makeApiCall();
	    }
	}

    /**
     * If Api is not downloading and has next page and ignore any previous error then make api call
     */
	public void downloadNextPageIfIdleAndIgnoreError() {
    	if(!mIsDownloading && mHasNextPage) {
		    makeApiCall();
	    }
	}

	@Override
	public void onResponse(Call<ResultList> call, Response<ResultList> response) {
		ResultList resultList = response.body();

		if(response.isSuccessful() && resultList != null) {
			mHasNextPage = resultList.getTotalPages() > mPage;
			mPage++;
		}
		if(mCallback != null) {
			mCallback.onResponse(call, response);
		}
		mIsDownloading = false;
		mLastCallHasError = false;
	}

	@Override
	public void onFailure(Call<ResultList> call, Throwable t) {
		if(mCallback != null) {
			mCallback.onFailure(call, t);
		}
		mIsDownloading = false;
		mLastCallHasError = true;
	}

	public void reset() {
    	mIsDownloading = false;
    	mLastCallHasError = false;
        mHasNextPage = true;
    	if(mResultListCall != null && !mResultListCall.isCanceled()) {
    		mResultListCall.cancel();
	    }
	}
}
