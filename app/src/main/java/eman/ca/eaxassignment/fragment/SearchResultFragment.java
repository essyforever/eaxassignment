package eman.ca.eaxassignment.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;

import eman.ca.eaxassignment.R;
import eman.ca.eaxassignment.activity.MovieDetailActivity;
import eman.ca.eaxassignment.api.GetListApiController;
import eman.ca.eaxassignment.api.GetListApiController.SearchResultState;
import eman.ca.eaxassignment.model.Movie;
import eman.ca.eaxassignment.model.ResultList;
import eman.ca.eaxassignment.recyclerview.AbstractRecyclerSection;
import eman.ca.eaxassignment.recyclerview.RecyclerViewAdapter;
import eman.ca.eaxassignment.sectiondata.SearchResultListGenerator;
import eman.ca.eaxassignment.utils.Const;
import eman.ca.eaxassignment.utils.SnackBarHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Esmond on 2018-08-29.
 */

public class SearchResultFragment extends Fragment implements Callback<ResultList>, SearchResultListGenerator.SearchResultInterface {

    private String mListId = "1";

    private RecyclerView mRecyclerView;
    private RecyclerViewAdapter mAdapter;
    private ProgressBar mProgressBar;
    private Snackbar mSnackbar;

    private GetListApiController mGetListApiController;

    public static SearchResultFragment newInstance() {
        
        Bundle args = new Bundle();
        
        SearchResultFragment fragment = new SearchResultFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_search_result, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initRecyclerView();
	    mGetListApiController = new GetListApiController();
	    mGetListApiController.setRetrofitCallback(this);

        doNewSearch(mListId, null);
    }

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		if (getActivity() == null) {
			return;
		}

    	inflater.inflate(R.menu.searchbar, menu);
        initSearchView(menu.findItem(R.id.menu_action_search));
    }

    private void initSearchView(final MenuItem searchItem) {
        if (searchItem != null) {
            SearchView searchView = (SearchView) searchItem.getActionView();
            if (searchView != null) {
                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String queryString) {
                        searchItem.collapseActionView();
                        mListId = queryString;
                        doNewSearch(mListId, null);
                        return true;
                    }

                    @Override
                    public boolean onQueryTextChange(String s) {
                        return false;
                    }
                });
            }
        }
    }

	private void initRecyclerView() {
        mProgressBar = getView().findViewById(R.id.progress_bar);
        mRecyclerView = getView().findViewById(R.id.list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);

        mAdapter = new RecyclerViewAdapter();
        mRecyclerView.setAdapter(mAdapter);

		mRecyclerView.addOnChildAttachStateChangeListener(new RecyclerView.OnChildAttachStateChangeListener() {
			@Override
			public void onChildViewAttachedToWindow(View view) {

			}

			@Override
			public void onChildViewDetachedFromWindow(View view) {
			    // Attempt to cancel the image downloading when the view is out of range
				View thumbnailImg = view.findViewById(R.id.thumbnail);
				if(thumbnailImg != null && thumbnailImg instanceof ImageView) {
					Picasso.get().cancelRequest((ImageView) thumbnailImg);
				}
			}
		});

		resetSections();
    }

    /**
     * Clear out old Sections and restart
     */
    private void resetSections() {
	    mAdapter.setSections(new ArrayList<AbstractRecyclerSection>());
    }

    /**
     * Only add new sections to the existing list of sections
     * @param resultList
     */
    private void loadSections(ResultList resultList) {
        mAdapter.addSections(SearchResultListGenerator.generateSections(resultList, this));
    }

    /**
     * FreshSearch: Show ProgressBar and hide RecyclerView
     * ShowResult: Hide ProgressBar and display RecyclerView
     * ShowFirstPageError: Hide ProgressBar and hide RecyclerView; SnackBar should be displayed with error message
     * @param state
     */
    private void updateUIByState(SearchResultState state) {
    	if(mSnackbar != null) {
		    mSnackbar.dismiss();
	    }
        switch (state) {
            case FreshSearch:
                mProgressBar.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.INVISIBLE);
                break;

            case ShowResult:
                mProgressBar.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);
                break;

            case ShowFirstPageError:
                mProgressBar.setVisibility(View.INVISIBLE);
                mRecyclerView.setVisibility(View.GONE);
                break;

            default:
                break;
        }
    }

    /**
     * Search Movie List base on listId, which should be integer.
     * SortBy is available on api but not in used in code
     * @param listId
     * @param sortBy
     */
    private void doNewSearch(String listId, String sortBy) {
        updateUIByState(SearchResultState.FreshSearch);
	    resetSections();
        mGetListApiController.doSearch(listId, 1, sortBy);
    }

    @Override
    public void onResponse(Call<ResultList> call, Response<ResultList> response) {
        ResultList resultList = response.body();

        if(response.isSuccessful()) {
            updateUIByState(SearchResultState.ShowResult);
            loadSections(resultList);
        } else {
	        updateUIByState(SearchResultState.ShowFirstPageError);
	        try {
		        JSONObject jObjError = new JSONObject(response.errorBody().string());
		        showSnackbarError(jObjError.optJSONArray("errors").getString(0));
	        } catch (Exception e) {
		        showSnackbarError(getString(R.string.error_network_please_try_again));
	        }
        }
    }

    @Override
    public void onFailure(Call<ResultList> call, Throwable t) {
    	if(mGetListApiController.getPage() == 1) {
		    updateUIByState(SearchResultState.ShowFirstPageError);
	    }
	    showSnackbarError(getString(R.string.error_network_please_try_again));
    }

    /**
     * Display Snackbar with errorMessage and a button option to Retry the Api
     * @param errorMessage
     */
    private void showSnackbarError(String errorMessage) {
	    mSnackbar = SnackBarHelper.showError(getView(), errorMessage, getString(R.string.retry), generateSnackBarOnClickListener());
    }

    private View.OnClickListener generateSnackBarOnClickListener() {
    	return new View.OnClickListener() {
		    @Override
		    public void onClick(View view) {
		    	updateUIByState(SearchResultState.FreshSearch);
				mGetListApiController.downloadNextPageIfIdleAndIgnoreError();
		    }
	    };
    }

    /**
     * If the user scrolls to the last 10 listings, we will try to download the next page of 20 movies to reach infinite scrolling
     * @param position
     */
    @Override
    public void shouldDownNextPage(int position) {
        if(mAdapter.getItemCount() - position < 10) {
	        mGetListApiController.downloadNextPageIfIdleAndNoError();
        }
    }

    /**
     * When the user clicks on a cell, movie, it will launch the MovieDetailActivity
     * @param movie
     */
	@Override
	public void onClick(Movie movie) {
		Intent intent = new Intent(getActivity(), MovieDetailActivity.class);
		intent.putExtra(Const.BUNDLE_MOVIE, movie);
		startActivity(intent);
	}
}
