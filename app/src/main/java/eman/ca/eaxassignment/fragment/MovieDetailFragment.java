package eman.ca.eaxassignment.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import eman.ca.eaxassignment.R;
import eman.ca.eaxassignment.model.Movie;
import eman.ca.eaxassignment.utils.Const;

/**
 * Created by Esmond on 2018-08-30.
 */

public class MovieDetailFragment extends Fragment {

	public static MovieDetailFragment newInstance(Bundle args) {
		if(args == null) {
			args = new Bundle();
		}

		MovieDetailFragment fragment = new MovieDetailFragment();
		fragment.setArguments(args);
		return fragment;
	}

	private ImageView mCoverArt;
	private TextView mTitle;
	private TextView mReleaseDate;
	private TextView mDescription;

	private Movie mMovie;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_movie_detail, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mCoverArt = getView().findViewById(R.id.movie_cover);
		mTitle = getView().findViewById(R.id.title);
		mReleaseDate = getView().findViewById(R.id.release_date);
		mDescription = getView().findViewById(R.id.description);

		mMovie = (Movie) getArguments().get(Const.BUNDLE_MOVIE);

		loadUI();
	}

	private void loadUI() {
		Picasso.get().load(Const.IMAGE_URL + mMovie.getBackdropPath()).into(mCoverArt);
		mTitle.setText(mMovie.getTitle());
		mReleaseDate.setText(mMovie.getReleaseDate());
		mDescription.setText(mMovie.getOverview());
	}

	@Override
	public void onDestroy() {
		Picasso.get().cancelRequest(mCoverArt);
		super.onDestroy();
	}
}
