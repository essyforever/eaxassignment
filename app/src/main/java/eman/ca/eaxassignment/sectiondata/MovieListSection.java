package eman.ca.eaxassignment.sectiondata;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import eman.ca.eaxassignment.R;
import eman.ca.eaxassignment.model.Movie;
import eman.ca.eaxassignment.recyclerview.AbstractRecyclerSection;
import eman.ca.eaxassignment.recyclerview.RecyclerViewType;
import eman.ca.eaxassignment.utils.Const;

/**
 * Created by Esmond on 2018-08-29.
 */

public class MovieListSection extends AbstractRecyclerSection {

    private Movie mMovie;
    private SearchResultListGenerator.SearchResultInterface mSearchResultInterface;

    public MovieListSection(Movie movie, SearchResultListGenerator.SearchResultInterface searchResultInterface) {
        super(RecyclerViewType.MOVIE_LIST_ITEM);
        mMovie = movie;
        mSearchResultInterface = searchResultInterface;
    }

    @Override
    public void bindSectionHeaderViewHolder(RecyclerView.ViewHolder sectionViewHolder, int sectionIndex) {
        MovieListViewHolder viewHolder = (MovieListViewHolder) sectionViewHolder;
        Picasso.get().load(Const.IMAGE_URL + mMovie.getPosterPath()).error(R.mipmap.ic_launcher).into(viewHolder.mThumbnailImgView);
        viewHolder.mTitleTextView.setText(mMovie.getTitle());
        viewHolder.mReleaseDateTextView.setText(mMovie.getReleaseDate());

        if(mSearchResultInterface != null) {
            mSearchResultInterface.shouldDownNextPage(sectionIndex);
        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
	        @Override
	        public void onClick(View view) {
		        if(mSearchResultInterface != null) {
		        	mSearchResultInterface.onClick(mMovie);
		        }
	        }
        });
    }

    @Override
    public RecyclerView.ViewHolder createViewHolder(ViewGroup parent) {
        return new MovieListViewHolder(createView(parent));
    }

    @Override
    public int getLayout() {
        return R.layout.view_movie_list;
    }

    public static class MovieListViewHolder extends RecyclerView.ViewHolder {

        private ImageView mThumbnailImgView;
        private TextView mTitleTextView;
        private TextView mReleaseDateTextView;

        public MovieListViewHolder(View itemView) {
            super(itemView);

            mThumbnailImgView = itemView.findViewById(R.id.thumbnail);
            mTitleTextView = itemView.findViewById(R.id.title);
            mReleaseDateTextView = itemView.findViewById(R.id.release_date);
        }
    }
}
