package eman.ca.eaxassignment.sectiondata;

import java.util.ArrayList;

import eman.ca.eaxassignment.model.Movie;
import eman.ca.eaxassignment.model.ResultList;
import eman.ca.eaxassignment.recyclerview.AbstractRecyclerSection;

/**
 * Created by Esmond on 2018-08-29.
 */

public class SearchResultListGenerator {

    public interface SearchResultInterface {
        void shouldDownNextPage(int position);
        void onClick(Movie movie);
    }

    public static ArrayList<AbstractRecyclerSection> generateSections(ResultList resultList, SearchResultInterface searchResultInterface) {
        ArrayList<AbstractRecyclerSection> sections = new ArrayList<>();
        if(resultList.getPage() == 1) {
            sections.add(new MovieListHeaderSection(resultList));
        }
        if(resultList.getMovieList() != null) {
            for(Movie movie : resultList.getMovieList()) {
                sections.add(new MovieListSection(movie, searchResultInterface));
            }
        }
        return sections;
    }
}
