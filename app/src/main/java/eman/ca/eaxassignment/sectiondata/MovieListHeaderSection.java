package eman.ca.eaxassignment.sectiondata;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import eman.ca.eaxassignment.R;
import eman.ca.eaxassignment.model.ResultList;
import eman.ca.eaxassignment.recyclerview.AbstractRecyclerSection;
import eman.ca.eaxassignment.recyclerview.RecyclerViewType;

/**
 * Created by Esmond on 2018-08-29.
 */

public class MovieListHeaderSection extends AbstractRecyclerSection {

    private ResultList mResultList;

    public MovieListHeaderSection(ResultList resultList) {
        super(RecyclerViewType.MOVIE_LIST_HEADER);

        mResultList = resultList;
    }

    @Override
    public void bindSectionHeaderViewHolder(RecyclerView.ViewHolder sectionViewHolder, int sectionIndex) {
        MovieListHeaderViewHolder viewHolder = (MovieListHeaderViewHolder) sectionViewHolder;

        viewHolder.mResultSummaryTextView.setText(viewHolder.itemView.getResources().getString(R.string.results_info, mResultList.getTotalResults()));
    }

    @Override
    public RecyclerView.ViewHolder createViewHolder(ViewGroup parent) {
        return new MovieListHeaderViewHolder(createView(parent));
    }

    @Override
    public int getLayout() {
        return R.layout.view_movie_list_header;
    }

    public static class MovieListHeaderViewHolder extends RecyclerView.ViewHolder {

        private TextView mResultSummaryTextView;

        public MovieListHeaderViewHolder(View itemView) {
            super(itemView);

            mResultSummaryTextView = itemView.findViewById(R.id.result_summary);
        }
    }
}
