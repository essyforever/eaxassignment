package eman.ca.eaxassignment.utils;

/**
 * Created by Esmond on 2018-08-30.
 */

public class Const {
	public static final String BUNDLE_MOVIE = "BundleMovie";

	public static final String BASE_URL = "https://api.themoviedb.org/4/";
	public static final String IMAGE_URL = "https://image.tmdb.org/t/p/w500";
	public static final String READ_ACCESS_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIyYTI1NTU4OGZkNjY4YWZhNTQ1NGE0ZTU1YmI3ZjYxZiIsInN1YiI6IjViODZkZThiOTI1MTQxNGEwZjAxMWJlZSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.BB_i8aJafFGxo4XtjmIVyhct7YxyC8aCLIkulhCqceo";
	public static final String CONTENT_TYPE = "application/json;charset=utf-8";
}
